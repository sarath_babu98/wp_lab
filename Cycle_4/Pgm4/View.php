<?php
    include "Dbconnect.php";
    $sql = "SELECT * FROM books";
    $result = $conn->query($sql);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>View</title>
</head>
<body>
    <table>
        <tr>
            <th>Book No</th>
            <th>Book Title</th>
            <th>Book Edition</th>
            <th>Book Publisher</th>
        </tr>
        <?php
            if($result->num_rows > 0) {
                while($row = $result->fetch_assoc()){
                    echo "<tr>
                                <td>" . $row['bookno'] . "</td>
                                <td>" . $row['title'] . "</td>
                                <td>" . $row['edition'] . "</td
                                <td>" . $row['publisher'] . "</td>
                            </tr";
                }
            }
        ?>
    </table>
</body>
</html>